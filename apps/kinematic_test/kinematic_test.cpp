#include "kinematic_model.h"
#include <iostream>

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  // For a small variation of q, compute the variation on X and check dx = J . dq  

  // Output cartesian position
  Eigen::Vector2d xOut;
  // Output jacobian matrix
  Eigen::Matrix2d JOut;
  // Joint position
  Eigen::Vector2d qIn (M_PI/3.0,M_PI_4);
  // Lengths
  const double l1In = 1;
  const double l2In = 1; 
  //Initialisation of the classe
  RobotModel MyRobotModel(l1In, l2In);
  //Call of the function FwdKin
  MyRobotModel.FwdKin(xOut, JOut, qIn);
  std::cout << " xOut = " << xOut << "\n " << std::endl;
  std::cout << " JOut = " << JOut << "\n " << std::endl;
}
