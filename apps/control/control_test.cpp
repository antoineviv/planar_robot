#include "control.h"
#include "trajectory_generation.h"
#include <iostream>


int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6

  Eigen::Vector2d q (M_PI_2,M_PI_4); // Joint position
  Eigen::Vector2d xd; // Desired cartesian position
  Eigen::Vector2d Dxd_ff; // Joint velocity feedforward term
  const double l1 = 0.4; // Lenght 
  const double l2 = 0.5; // Lenght
  Eigen::Vector2d X_i; // Initial position
  Eigen::Matrix2d J; // Jacobian matrix
  const double DtIn = 1; //Time interval
  const Eigen::Vector2d X_f (0.0,0.6); //Final position (x,y)

  //Initialisation of the classes
  RobotModel MyRobotModel(l1, l2);
  Controller MyController(MyRobotModel);
  MyRobotModel.FwdKin(X_i,J,q);
  Point2Point MyTrajectory(X_i,X_f,DtIn);
  float t;
  float dt = 0.01;

  std::cout << "t,q1,q2,xd,yd,x,y \n"; // Datas for the plot juggler


  for(t=0;t<DtIn;t+=dt)
  {
    xd = MyTrajectory.X(t); // Desired position
    Dxd_ff = MyTrajectory.dX(t); // Joint velocity feedforward term
    Eigen::Vector2d Dqd = MyController.Dqd(q,xd,Dxd_ff); //Joint velocities
    MyRobotModel.FwdKin(X_i,J,q); // Forward kinematics
    // Datas for the plot juggler
    std::cout << t << "," << q(0) << "," << q(1) << "," << xd(0) << "," << xd(1) << "," << X_i(0) << "," << X_i(1) << "\n";

    q = Dqd*dt + q; // New joint position
  }

}