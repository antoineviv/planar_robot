#include "trajectory_generation.h"
#include <iostream>


int main(){

  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities

  //Initial position
  const double piIn = 0;   //Initial position
  const double pfIn = 1; //Final position
  const double DtIn = 1; //Time interval
  const double t = 0; //Specific time
  const Eigen::Vector2d X_i (0,0); //Initial position (x,y)
  const Eigen::Vector2d X_f (1,1); //Final position (x,y)
  Eigen::Vector2d X1; //Current positon 
  Eigen::Vector2d X2; //Next position
  Eigen::Vector2d dXa; //Analytical velocity
  Eigen::Vector2d dXn; //Numerical velocity

  //Initialisation of the classes
  Polynomial MyPolynomial(piIn, pfIn, DtIn);
  Point2Point MyTrajectory(X_i, X_f, DtIn);

  //Call of the function update
  MyPolynomial.update(piIn, pfIn, DtIn);
  //Position and velocity at a given time
  MyPolynomial.p(t);
  MyPolynomial.dp(t);

  //dt is the time interval between 2 positions
  double dt;
  for(dt=0;dt<DtIn;dt+=0.01)
  {
    //Coordinates x and y of the current position and the next one
    X1 = MyTrajectory.X(dt);
    X2 = MyTrajectory.X(dt+0.01);
    //Analytical velocity of the current position
    dXa = MyTrajectory.dX(dt);
    //Numerical velocity between the current position and the next one
    dXn(0) = (X2(0)-X1(0))/0.01;
    dXn(1) = (X2(1)-X1(1))/0.01;

    std::cout << " At T = " << dt << "\n " << std::endl;
    std::cout << " The velocity analytical is " << dXa << "\n " << std::endl;
    std::cout << " The velocity numerical is " << dXn << "\n " << std::endl;
  }

}
