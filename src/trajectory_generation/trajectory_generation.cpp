#include "trajectory_generation.h"
#include <iostream>

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;
  a[0] = pi;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = 15*(pi-pf);
  a[5] = 6*(pf-pi);
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;
  a[0] = pi;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = 15*(pi-pf);
  a[5] = 6*(pf-pi);
};

const double  Polynomial::p     (const double &t){
  //TODO compute position
  //Fifth degree polynomial with the coefficients
  double p = a[0] + a[1]*(t/Dt) + a[2]*pow(t/Dt,2) + a[3]*pow(t/Dt,3) + a[4]*pow(t/Dt,4) + a[5]*pow(t/Dt,5);
  return p;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  //Derivative of fifth degree polynomial with the coefficients
  double dp = a[1]/Dt + 2*a[2]*(t/pow(Dt,2)) + 3*a[3]*(pow(t,2)/pow(Dt,3)) + 4*a[4]*(pow(t,3)/pow(Dt,4)) + 5*a[5]*(pow(t,4)/pow(Dt,5));
  return dp;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  //TODO initialize object and polynomials
  //Coordinates x of the initial and final point
  polx.update(xi(0),xf(0),DtIn);
  //Coordinates y of the initial and final point
  poly.update(xi(1),xf(1),DtIn);
}

Eigen::Vector2d Point2Point::X(const double & time){
  //TODO compute cartesian position
  //Vector with coordinates x and y at a given time
  Eigen::Vector2d Pos(polx.p(time),poly.p(time));
  return Pos;
}

Eigen::Vector2d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  //Vector with the velocity at a given time 
  Eigen::Vector2d dPos(polx.dp(time),poly.dp(time));
  return dPos;
}